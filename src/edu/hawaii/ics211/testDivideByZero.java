/**
 * 
 */
package edu.hawaii.ics211;

/**Test dividing by zero.
 * @author Geryll Anthony Agno
 *
 */
public class testDivideByZero {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Calculator calculator = new Calculator();
		
		System.out.println(calculator.divide(123, 0));
	}

}
