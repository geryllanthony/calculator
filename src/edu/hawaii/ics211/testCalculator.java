/**
 * CtestCalculator.java
 * Copyright (c) Geryll Anthony Agno 2016
 */
package edu.hawaii.ics211;

/**
 * Test class for the Calculator.java class
 * @author Geryll Anthony Agno
 *
 */
public class testCalculator {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Calculator basicCalculator = new Calculator();
		
		System.out.println(basicCalculator.add(0, 0));
		System.out.println(basicCalculator.add(0, 12389));
		System.out.println(basicCalculator.add(1235, 0));
		System.out.println(basicCalculator.add(-123154, 0));
		System.out.println(basicCalculator.add(-1, -2));
		
		System.out.println();
		System.out.println(basicCalculator.subtract(0, -123));
		System.out.println(basicCalculator.subtract(0, 0));
		System.out.println(basicCalculator.subtract(-23, -25));
		System.out.println(basicCalculator.subtract(-123, 2));
		System.out.println(basicCalculator.subtract(123, 3));
		
		System.out.println();
		System.out.println(basicCalculator.multiply(0, -123));
		System.out.println(basicCalculator.multiply(0, 3));
		System.out.println(basicCalculator.multiply(0, 0));
		System.out.println(basicCalculator.multiply(-23, -25));
		System.out.println(basicCalculator.multiply(-123, 2));
		System.out.println(basicCalculator.multiply(123, 3));
		
		System.out.println();
		System.out.println(basicCalculator.divide(0, -123));
		System.out.println(basicCalculator.divide(-23, -23));
		System.out.println(basicCalculator.divide(-123, 2));
		System.out.println(basicCalculator.divide(123, 3));
		System.out.println(basicCalculator.divide(0, 1));
		System.out.println(basicCalculator.divide(123, -54));
		
		System.out.println();
		System.out.println(basicCalculator.modulo(6, 3));
		System.out.println(basicCalculator.modulo(-23, -23));
		System.out.println(basicCalculator.modulo(5, -4));
		System.out.println(basicCalculator.modulo(-21, 8));
		System.out.println(basicCalculator.modulo(0, 1));
		System.out.println(basicCalculator.modulo(123, -54));
		
		System.out.println();
		System.out.println(basicCalculator.pow(2, 2));
		System.out.println(basicCalculator.pow(123, 0));
		System.out.println(basicCalculator.pow(2, -4));
		System.out.println(basicCalculator.pow(-5, 5));
		System.out.println(basicCalculator.pow(0, 9454));
		System.out.println(basicCalculator.pow(-5, -7));
	}

}
