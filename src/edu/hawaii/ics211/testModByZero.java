/**
 * 
 */
package edu.hawaii.ics211;

/**Test modulo by zero.
 * @author Geryll Anthony Agno
 *
 */
public class testModByZero {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Calculator calculator = new Calculator();
		
		System.out.println(calculator.modulo(123, 0));
	}

}
